import sys
import os.path

INCP = '>'
DECP = '<'
INCT = '+'
DECT = '-'
OUT = '.'
IN = ','
JMPIFZ = '['
JMPIFNZ = ']'
_ALLOWED_SYMBOLS = {INCP, DECP, INCT, DECT, OUT, IN, JMPIFZ, JMPIFNZ}

def license():
    print ('YABFI - Yet Another BrainFuck Interpreter - 1.0\n'
           'Copyright (C) 2015 - Nejc Trdin\n\n'    
           'This program is free software: you can redistribute it and/or modify\n'
           'it under the terms of the GNU General Public License as published by\n'
           'the Free Software Foundation, either version 3 of the License, or\n'
           '(at your option) any later version.\n\n'    
           'This program is distributed in the hope that it will be useful,\n'
           'but WITHOUT ANY WARRANTY; without even the implied warranty of\n'
           'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n'
           'GNU General Public License for more details.\n\n'    
           'You should have received a copy of the GNU General Public License\n'
           'along with this program.  If not, see <http://www.gnu.org/licenses/>.\n')
    print 
    usage()

def usage():
    print 'YABFI callable as "python interpreter.py [-d][-m][-s][-h] program"'
    print 
    print ('This is a brainfuck interpreter written in python.\n\nIt reads the input '
           'program from the command line argument or from a file.\n\nIts tape is '
           'possibliy infinite in both directions.\n\nInitially it allocates a tape of '
           'size 10. If the pointer goes out of the tape, the tapes size is enlarged by '
           'a factor of 2.\n')
    
def run(program, debug, memoryDump, simulate):
    tape = [0] * 10
    tapePointer = 0

    programPointer = -1

    while programPointer < len(program) - 1:
        programPointer += 1

        symbol = program[programPointer]

        if symbol not in _ALLOWED_SYMBOLS:
            continue

        if debug:
            print (symbol, tapePointer, tape)
       
        if symbol == INCP:
            tapePointer += 1
            if tapePointer == len(tape):
                tape = tape + [0] * len(tape)
        elif symbol == DECP:
            tapePointer -= 1
            if tapePointer == -1:
                tapePointer += len(tape)
                tape = [0] * len(tape) + tape
        elif symbol == INCT:
            tape[tapePointer] += 1
        elif symbol == DECT:
            tape[tapePointer] -= 1
        elif symbol == OUT:
            if not simulate:
                sys.stdout.write(chr(tape[tapePointer]))
                if debug:
                    print
        elif symbol == IN:
            value = raw_input()
            tape[tapePointer] = int(value)
        elif symbol == JMPIFZ:
            if tape[tapePointer] == 0:
                debugPointer = tapePointer
                opened = 1
                while opened:
                    programPointer += 1
                    if programPointer == len(program):
                        print 'Error in program. Could not find mathcing "]"!'
                        print ' '.join(["c:", str(debugPointer)])
                        sys.exit(3)
                    if program[programPointer] == JMPIFZ:
                        opened += 1
                    elif program[programPointer] == JMPIFNZ:
                        opened -= 1
        elif symbol == JMPIFNZ:
            if tape[tapePointer]:
                debugPointer = tapePointer
                opened = 1
                while opened:
                    programPointer -= 1
                    if programPointer == -1:
                        print 'Error in program. Could not find mathcing "["!'
                        print ' '.join(["c:", str(debugPointer)])
                        sys.exit(4)
                    if program[programPointer] == JMPIFZ:
                        opened -= 1
                    elif program[programPointer] == JMPIFNZ:
                        opened += 1
    if memoryDump:
        print tape

def runYABFI():
    if '-h' in sys.argv:
        license()
        sys.exit(0)
    if len(sys.argv) < 2 or len(sys.argv) > 5:
        print 'Incorrect number of arguments!'
        usage()
        sys.exit(1)

    debug = False
    memoryDump = False
    simulate = False

    if '-d' in sys.argv:
        debug = True
    if '-m' in sys.argv:
        memoryDump = True
    if '-s' in sys.argv:
        simulate = True

    program = sys.argv[len(sys.argv) - 1]
    if os.path.isfile(program):
        f = file(program, 'r')
        program = f.read()

    run(program, debug, memoryDump, simulate)

if __name__ == "__main__":
    runYABFI()
