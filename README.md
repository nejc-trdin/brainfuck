YABFI - Yet Another BrainFuck Interpreter
===========
This is a brainfuck interpreter written in python. It reads the input program from the command line argument or from a file supplied in the `program` argument. Its tape is possibliy infinite in both directions. Initially it allocates a tape of size 10. If the pointer goes out of the tape, the tape's size is enlarged by a factor of 2.

## Usage

    python interpreter.py [-d][-m][-s][-h] program
        -d      debug the run, in each step it gives the symbol, tape pointer and tape variables
        -m      dump the tape to the stdout after finishing the execution
        -s      simulate running, but do not output anything
        -h      print the help


### Example
The hello world program can be run:
```sh
$ python interpreter.py "++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++."
Hello World!
```

## Getting the source
Download the source code by running the following code in your command prompt:
```sh
$ git clone git@gitlab.com:nejc-trdin/brainfuck.git
```

Or get the latest archive by [downloading](https://gitlab.com/nejc-trdin/brainfuck/repository/archive.zip) it. 


## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see [GNU License](http://www.gnu.org/licenses/).

## Contributors
    Nejc Trdin https://gitlab.com/nejc-trdin nejc.trdin@gmail.com
